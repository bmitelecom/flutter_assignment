# Flutter Assignment



## Getting started

This assignmnent covers many areas each Flutter-developer has to comply with. It's not tricky and extremely biased, so just try to show your best.

What you have to do (shortly):
- Create your own public GIT-repo on a platform of your preference (Github, Gitlab, etc)
- Create a new Flutter project;
- Create a structure of the project to follow any architectural paradigm you prefer;
- Create necessary screns, widgets, controllers, etc for the provided test screen;
- Commit all the changes to your repository;
- We are not limit you in anything - create files, assets, anything that might be of help to show your abilities;
- Make sure that project can be build and tested without any issues;
- Count the number of hours you spent on this entire project.

## Sample screen

![Semantic description of image](/Main.png "Sample Screen")
